get_ci_info(){
curl -s -f -X GET --header 'Content-Type: application/json;charset=UTF-8' "https://gitee.com/api/v5/repos/src-openeuler/$name/pulls/$prnum/comments?access_token=$token&page=1&per_page=20" -o .file || { continue;}
python3 a.py
}

create_ci_info_deal(){
cat > a.py <<XXX
import json;
json_file = open(".file", mode="r")
data = json.load(json_file)

for i in range(len(data)-1,-1,-1):
    try:
        ci_info = data[i]["body"]
        if "task-check-code-style" in ci_info:
            break
        else:
            ci_info=""
    except IndexError:
        continue
if len(ci_info) == 0:
    print("UNBUILD")
    exit()

info_split = ci_info.split("|")
ci_info_1 = info_split[10].split("*")[2]
ci_info_2 = info_split[14].split("*")[2]
ci_info_3 = info_split[18].split("*")[2]
if ci_info_1 == "SUCCESS" and ci_info_2 == "SUCCESS" and ci_info_3 == "SUCCESS":
    print("SUCCESS")
else:
    print("FAILURE")
XXX
}

main(){
create_ci_info_deal
while read x;do
	cut=$(echo $x | awk '{print NF}')
	if [[ $cut -ge 4 ]];then
	    name=$(echo $x |awk '{print $1}')
	    prnum=$(echo $x |awk '{print $2}' |awk -F '/' '{print $7}')
	    echo -n "$x "
	    get_ci_info
	else 
            echo "$x"
	fi
done < $file | tee b.csv
rm -f a.py
sed -i 's/ /,/g' b.csv
}

file=$1
token=$2
main
