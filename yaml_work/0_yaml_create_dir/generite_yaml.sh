#!/bin/bash

gen_yaml(){
cat > $DD/$1.yaml << XXX
version_control: $2
src_repo: $3
tag_prefix: $4
seperator: $5
XXX
}

main(){

f_c=0
s_c=0
file=$1

DD="yaml_dir"
if [[ ! -d $DD ]];then
	mkdir -p $DD
fi


while read line ;do
	array=($(echo $line))
	if [[ 5 != ${#array[@]} ]];then
		((f_c++))
		continue
	fi

	gen_yaml "${array[@]}"
	((s_c++))

done < <(cat $file)

echo "RESULT :: Failed ${f_c} Succed ${s_c}"
}
main $@
