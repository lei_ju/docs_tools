#!/bin/bash
if [[ $# == 0 ]];then
	echo "$0 GITEE_ID PKG_NAME API_TOKEN YAML "
	echo "EXAMPLE: $0 sugarfillet python-parse '12341341' python-parse.yaml"
	exit 2
fi

gitee_id=$1
name=$2
token=$3
msg="add yaml file"

# do git fork 
curl -f -X POST --header 'Content-Type: application/json;charset=UTF-8' "https://gitee.com/api/v5/repos/src-openeuler/$name/forks" -d "{\"access_token\":\"$token\"}" &>/dev/null && echo "Hint: $name fork yes"
sleep 3
# sync fork repo
git clone git@gitee.com:${gitee_id}/${name}.git 
# cp $@ to the local repo
shift 3
cp -f "$@" ${name}
# git * 
pushd ${name} &>/dev/null
#rm -rf README*
git add -A
git commit -m "$msg"
git push origin master:master -f
popd &>/dev/null
# do git pr 
curl -f -X POST --header 'Content-Type: application/json;charset=UTF-8' "https://gitee.com/api/v5/repos/src-openeuler/${name}/pulls" -d "{\"access_token\":\"$token\",\"title\":\"$msg\",\"head\":\"$gitee_id:master\",\"base\":\"master\"}" &>/dev/null &&  echo "Hint: $name PR yes"

