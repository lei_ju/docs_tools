changelog_update(){
# changelog replace                                                                 
sed -i '/^%changelog/,$d' $file                                                     
date=`date +"%a %b %_d %Y"`                                                         
name='Shuaishuai Song'                                                              
email='songshuaishuai2@huawei.com'                                                  
changelog="%changelog\n* $date $name <$email> - ${version}-$release\n- package init"
echo -e $changelog >> $file #sed -n '/^%changelog/,$p' $file                        
echo "Hint: changelog updated"                                       
}


single_merge(){
awk -v key="$1" -v max_len=80 -F : \
'BEGIN {\
tmp="";\
} \
{\
	if( key == $1 ){\
		if( NF > 2){\
			oofs=FS;FS="#";
			bbb=sub(/[[:alnum:] ]+:/,"",$0);\
			aaa=$bbb;	
			FS=oofs;
		}\
		else{\
			$1=""; \
			aaa=$0;\
		}\

		tmp1=tmp aaa
		gsub(" +"," ",tmp1);\
		#print FNR tmp1
                if(length(tmp1) > max_len){\
			gsub(" +"," ",tmp);\
			printf "%-20s%s\n",key ":",tmp;\
			tmp="";\
		}\
		tmp=tmp aaa;\
        }\
	else {\
		if( length(tmp) != 0){\
			gsub(" +"," ",tmp);\
			printf "%-20s%s\n",key ":",tmp;\
			tmp="";\
		}\
		print $0;\
        }\

}' 
}

headers_merge(){
	cat $file | single_merge "BuildRequires" | single_merge "Requires" > .$file
	mv -f .$file $file
	echo "Hint: Tags merged"                                       
}

delete_comment(){
	sed -i '/^#.*/d' $file
	echo "Hint: commentline deleted"
}
delete_blank(){
	sed -i 's/[[:blank:]]\{1,\}$//g' $file
	echo "Hint: tail-blank deleted "
}

insert_enter(){
	sed -i '/^$/d' $file
	sed -i '/^%package.*/i\ ' $file
	sed -i '/^%pre.*/i\ ' $file
	sed -i '/^%post.*/i\ ' $file
	sed -i '/^%build.*/i\ ' $file
	sed -i '/^%install.*/i\ ' $file
	sed -i '/^%check*/i\ ' $file
	sed -i '/^%files.*/i\ ' $file
	sed -i '/^%changelog.*/i\ ' $file
	echo "Hint: insert newline before stages"
}

increase_release(){
	# release +1
	sed -i "/^Release.*$/s/${release_oo}/${release}/" $file
	# delete Group Tags
	sed -i '/^Group.*/d' $file                            
	echo "Hint: release ++"
}

header_align() {
awk '{
if ($0 ~ /^[A-Z][[:alnum:]]*: / ) {
va="";
for(i=2;i<=NF;i++){ va=va" "$i};
printf("%-20s%s\n",$1,va);
}
else print $0;
}' $file > .$file
mv -f .$file $file
echo "Hint: header_align done" 
}

#################### main
main(){

file_o=$1          
file=${file_o}.cp    
cp -f $file_o $file

# got verison release                                                               
version=`cat $file |grep '^Version' | awk '{print $2}'`                             
release_oo=`cat $file |grep '^Release' | awk '{print $2}'`
release_o=`echo $release_oo | sed 's/%{.*}//'`
release=$((release_o+1))                                                                       

#marcos_expand keep this bug ...
delete_comment
changelog_update
increase_release
header_align
headers_merge
insert_enter
delete_blank

#let's play
read -p "Hint: exec vimdiff $file $file_o ? enter [yes/no]: " ans
vimdiff $file $file_o
read -p "Hint: do mv -b -f $file $file_o ? enter [yes/no]: " ans
if [[ $ans =~ [Yy]([Ee][Ss])? ]];then 
	mv -b -f $file $file_o
	echo "Hint: $file_o updated"
else
	rm -f $file
	echo "Hint: $file deleted. Not my bad :P "
fi
echo "From SONG: Enjoy your spec working !"
}

banner(){
cat <<eof
+-+-+-+-+-+-+-+-+-+
|s|p|e|c|_|p|r|e|p|
+-+-+-+-+-+-+-+-+-+
eof
}
if [[ $# != 1 ]];then
	banner
	echo "USAGE: $(basename $0) SPECFILE"
	exit 2 
fi
banner
main $1
