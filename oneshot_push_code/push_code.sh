#!/bin/bash
if [[ $# == 0 ]];then
	echo "$0 GITEE_ID PKG_NAME API_TOKEN REVIEWER PKG_TAR PKG_SPEC [PKG_PATCH...] "
	echo "EXAMPLE: $0 sugarfillet python-parse '12341341' yanzh_h parse-2.tar.gz python-parse.spec "
	exit 2
fi

gitee_id=$1
name=$2
token=$3
reviewer=$4

# do git fork 
curl -X POST --header 'Content-Type: application/json;charset=UTF-8' "https://gitee.com/api/v5/repos/src-openEuler/$name/forks" -d "{\"access_token\":\"$token\"}" &>/dev/null && echo "fork yes"
# sync fork repo
echo -n "ready to clone ? "
read do_clone 
git clone git@gitee.com:${gitee_id}/${name}.git 
# cp $@ to the local repo
shift 4
cp -f $@ ${name}
# git * 
pushd ${name} &>/dev/null
rm -rf README*
git add -A
git commit -m "package init"
echo -n "read to push ?"
read do_push
git push
popd &>/dev/null
# do git pr 
echo -n "read to pr ?"
read do_pr
curl -X POST --header 'Content-Type: application/json;charset=UTF-8' "https://gitee.com/api/v5/repos/src-openeuler/${name}/pulls" -d "{\"access_token\":\"$token\",\"title\":\"package init \",\"head\":\"$gitee_id:master\",\"base\":\"master\",\"assignees\":\"${reviewer}\"}" &>/dev/null && echo "PR yes"

