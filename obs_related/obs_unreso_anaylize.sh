#!/bin/bash
cang='openEuler:function'
repo='standard_aarch64'
arch='aarch64'
repo_n='abcded.repo'

if [[ ! -d msgs ]];then
	mkdir msgs
fi

# get all unresolved pkg
osc pr $cang -s U -r $repo -a $arch -q -V | sed 1d \
| sed '$d' | sed '$d'| awk '{print $2}' > unresolved_pkgs

# get pkgs' error msg
for x in `cat unresolved_pkgs`;do
	osc r $cang $x -r $repo -a $arch -v |grep -v unresolv \
| awk '{print $3}' >  msgs/$x.msg
done 

# config fedora & fedora-update source 
pushd /etc/yum.repos.d/
cat > $repo_n <<eof
[fedora-u]
name=fedora-u
baseurl=http://mirrors.aliyun.com/fedora/updates/29/Everything/aarch64
enabled=1
gpgcheck=0
priority=5

[fedora]
name=fedora
baseurl=http://mirrors.aliyun.com/fedora/releases/29/Everything/aarch64/os
enabled=1
gpgcheck=0
priority=4
eof 
popd

# do query 
printf "%s\t%s\t%s\t%s\n" PKG DEP BIN SRC
for x in `cat unresolved_pkgs`;do
	for y in `cat msgs/$x.msg`;do
	bin=`dnf repoquery --whatprovides "$y" --repo fedora --repo fedora-u -q |head -n1 | sed 's/-[[:digit:]]\+.*//'`
	src=`dnf repoquery -q -i $bin --repo fedora --repo fedora-u |grep Source| head -n1 | awk '{print $3}'| sed 's/-[[:digit:]]\+.*//'`
	printf "%s\t%s\t%s\t%s\n" $x $y $bin $src
	done
done | tee result.txt

rm -rf /etc/yum.repos.d/$repo_n
echo "=========================="
echo "PLEASE check the result.txt"
echo "=========================="

